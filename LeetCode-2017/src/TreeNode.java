/**
 * Created by zhang on 2018/1/26.
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int value){
        this.val = value;
    }
}
