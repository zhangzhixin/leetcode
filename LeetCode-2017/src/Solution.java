import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        class Server {
            private Integer id;
            Server(Integer _id) {
                id = _id;
            }
        }
        class ServersPool {
            private List<Server> serverList;
            private Map<Integer, Integer> serverDictionary;
            ServersPool() {
                serverList = new ArrayList<>();
                serverDictionary = new HashMap<>();
            }
            public Server getServer(Integer id) {
                Integer index = serverDictionary.get(id);
                if (index == null) {
                    return null;
                }
                return serverList.get(serverDictionary.get(index));
            }
            public Server getServer() {
                if (serverList.isEmpty()) return null;
                return serverList.get(new Random().nextInt(serverList.size()));
            }
            public Server deleteServer(Integer id) {
                Integer index = serverDictionary.get(id);
                if (index == null) {
                    return null;
                }
                Server temp = serverList.get(index);
                if (index != serverList.size() - 1) {
                    serverList.set(index, serverList.get(serverList.size() - 1));
                    serverList.remove(serverList.size() - 1);
                    serverDictionary.remove(temp.id);
                    serverDictionary.put(serverList.get(index).id, index);
                }
                else {
                    serverList.remove(serverList.size() - 1);
                    serverDictionary.remove(temp.id);
                }
                return temp;
            }
            public boolean addServer(Server server) {
                if (serverDictionary.get(server.id) != null) {
                    return false;
                }
                serverList.add(server);
                serverDictionary.put(server.id, serverList.size() - 1);
                return true;
            }
        }

        Server s1, s2, s3, s4;
        s1 = new Server(1);
        s2 = new Server(2);
        s3 = new Server(3);
        s4 = new Server(4);

        ServersPool pool = new ServersPool();
        pool.addServer(s1);
        pool.addServer(s2);
        System.out.println(pool.getServer().id);
        System.out.println(pool.getServer().id);
        System.out.println(pool.getServer().id);
        System.out.println(pool.getServer().id);
        System.out.println(pool.getServer().id);
        pool.deleteServer(1);
        System.out.println(pool.getServer().id);
        System.out.println(pool.getServer().id);
        System.out.println(pool.deleteServer(1));
        System.out.println(pool.deleteServer(2));
        System.out.println(pool.getServer());
        System.out.println(pool.getServer(2));
        System.out.println(pool.addServer(s3));
        System.out.println(pool.addServer(s3));
    }
}