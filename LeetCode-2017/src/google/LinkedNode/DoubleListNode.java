package google.LinkedNode;

/**
 * Created by zhang on 2018/8/11.
 */
public class DoubleListNode {
    int val;
    DoubleListNode next;
    DoubleListNode prev;
    public DoubleListNode(int val){
        this.val = val;
        next = null;
        prev = null;
    }
}
